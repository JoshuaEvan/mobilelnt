package com.praetbncc.praetbnccpert2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.praetbncc.praetbnccpert2.R;

import java.lang.reflect.Method;

public class MainActivity extends AppCompatActivity {

    private TextView tv_username;
    private TextView tv_password;
    private EditText et_username;
    private EditText et_password;
    private EditText et_age;
    private EditText et_gender;
    private Button btn_register;


//
//    I have had recently two telephone interviews where I've been asked about the differences between an
//      Interface and an Abstract class. I have explained every aspect of them I could think of
//  , but it seems they are waiting for me to mention something specific, and I don't know what it is.
//
//    From my experience I think the following is true. If I am missing a major point please let me know.
//
//            Interface:
//
//    Every single Method declared in an Interface will have to be implemented in the subclass.
//      Only Events, Delegates, Properties (C#) and Methods can exist in a Interface. A class can implement multiple Interfaces.
//
//    Abstract Class:
//
//    Only Abstract methods have to be implemented by the subclass.
//     An Abstract class can have normal methods with implementations. Abstract class can also have class variables beside Events,
//      Delegates, Properties and Methods. A class can only implement one abstract class only due non-existence of Multi-inheritance in C#.
//
//    After all that, the interviewer came up with the question
//    "What if you had an Abstract class with only abstract methods?
//     How would that be different from an interface?" I didn't know the answer but I think it's the inheritance as mentioned above right?
//
//    An another interviewer asked me what if you had a Public variable inside the interface, how would that be different than in Abstract Class? I insisted you can't have a public variable inside an interface. I didn't know what he wanted to hear but he wasn't satisfied either.


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //kenapa kok perlu di type cast ke edittext karena dia retur nya itu dalam bentuk keals View which is parent dari editText
        setContentView(R.layout.activity_main);
        et_password=findViewById(R.id.et_password);
        et_username=findViewById(R.id.et_username);
        et_age=findViewById(R.id.et_age);
        et_gender=findViewById(R.id.et_gender);
        btn_register= findViewById(R.id.btn_register);
        Toast toast= Toast.makeText(this,"Hai",Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.AXIS_PULL_AFTER     | Gravity.CENTER_HORIZONTAL,0,0);
        toast.show();
       btn_register.setOnClickListener( new View.OnClickListener(){
           public void onClick(View view){

           }
        });
        btn_register.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
                //showToast();
            }
        }));

    }

    private void validate(){

        boolean valid=true;
        if(et_username.getText().toString().isEmpty()){
            Toast.makeText(this, "Username cannot be empty", Toast.LENGTH_SHORT).show();
            valid=false;
        }


        if(et_password.getText().toString().isEmpty()){
            Toast.makeText(this, "Password cannot be empty", Toast.LENGTH_SHORT).show();
            valid=false;
        }else if(et_password.getText().toString().length() < 8){
            Toast.makeText(this, "Password must be at least 8 characters", Toast.LENGTH_SHORT).show();
            valid=false;
        }


        if(et_age.getText().toString().isEmpty()){
            Toast.makeText(this, "Age cannot be empty", Toast.LENGTH_SHORT).show();
            valid=false;
        }else if(Integer.parseInt(et_age.getText().toString()) < 10){
            Toast.makeText(this, "Insufficient age", Toast.LENGTH_SHORT).show();
            valid=false;
        }

        if(!et_gender.getText().toString().equals("Female") && !et_gender.getText().toString().equals("Male")){
            valid=false;
        }

        if(valid){
            Toast.makeText(this, "Successfully registered", Toast.LENGTH_SHORT).show();
        }
    }
//    private void showToast(){
//        Toast.makeText(MainActivity.this,"Your Username : "+et_username.getText().toString(), Toast.LENGTH_SHORT).show();
//        Toast.makeText(MainActivity.this,"Your Password : "+et_password.getText().toString(),Toast.LENGTH_SHORT).show();
//
//    }

}
